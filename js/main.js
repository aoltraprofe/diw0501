$(function () {
    // activación de tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // Animación para ir suavemente a los enlaces de la barra y desde abajo hacia arriba
    $(".navbar a, footer a[href='#home']").on('click', function (event) {

        // En caso de que exista para de ancla (#) en la referencia
        if (this.hash !== "") {

            event.preventDefault();

            // guardo el valor del ancla
            var hash = this.hash;

            // Animo el scroll con jQuery
            $('html, body').animate({

                scrollTop: $(hash).offset().top - 40

            }, 900, function () {

                // Cuando acabe la animación referencio la ubicacion de la página en el ancla almacenado
                window.location.hash = hash;

            });
        }
    });


});